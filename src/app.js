import React from 'react';
import Header from './components/Header'
import MainInfo from './components/MainInfo'
const App = () => {
  return (
    <>
      <Header />
      <MainInfo />
    </>
  );
};

export default App;