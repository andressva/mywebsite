import React from 'react';

const Header = () => {
  return (
      <div style={{
          display: "flex",
          alignItems: "center",
          paddingLeft: "10px",
          width: "100%",
          height: "80px",
          backgroundColor: "white",
          borderBottom: "1px solid gray",
          fontFamily: "Arial"
      }}>
        <h2>My Website</h2>
      </div>
  );
};

export default Header;