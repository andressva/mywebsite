import React from 'react';

const MainInfo = () => {
  return (
      <div style={{
          display: "flex",
          flexDirection: "column",
          paddingLeft: "10px",
          width: "100%",
          height: "350px",
          backgroundColor: "#005073",
          color: "white",
          borderBottom: "1px solid gray",
          fontFamily: "Arial"
      }}>
        <p style={{
            fontWeight: "bold"
        }}>My name is, Andres Vergel Alvarez</p>
        <p style={{
            fontWeight: "bold"
        }}>Dolor officia officia ex irure qui dolore duis eu aute aliquip nisi eu. Amet ullamco mollit voluptate fugiat sunt. Commodo sint proident excepteur aliquip ipsum duis labore incididunt. Occaecat ex ullamco veniam reprehenderit deserunt dolore incididunt commodo anim in proident velit incididunt. Elit dolor commodo pariatur cillum ut amet ipsum dolor ex. Enim labore irure ut incididunt veniam cillum ullamco elit est exercitation officia aliqua cillum.</p>
      </div>
  );
};

export default MainInfo;